function out = ZFB(x,par)
    Cfb = 4e-12;
    CfbZ = complex(0,x*2*pi*Cfb);
    den = 1./par(4) + CfbZ;
    out = 4./den;
end