function out = HEMTSquaredGain(x,par)
    HEMTSquaredVec = HEMTSquared(x);
    out = HEMTSquaredVec*abs(amp_gain(x,par)).^2;
end