function out = generateSigToNoisePlot(params, freq1_25e6)
    PSD = arrayfun(@(x) NoiseSQTot_JohnsonSqared_HEMTSquared(x,params), freq1_25e6);
    signal = arrayfun(@(x) Vout(x, params), freq1_25e6);
    %figure
    %loglog(freq1_25e6, abs(signal).^2./PSD, '.');
    out = abs(sqrt(19.07)*signal).^2./PSD;
    %one of these 2s is from the noise ampltide (which should be halfed
    %when everything is averaged)
    %the other 2 is from the sum of the OF integral extending down to
    %negative frequencies
    expectedENC_RMS = sqrt(1/sum(2*2*out(2:end)))/1.6e-19
end