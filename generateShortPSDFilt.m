function PSD_short = generateShortPSD(PSDlong, H)
    size1 = size(PSDlong);
    size2 = max(size1(1), size1(2));
    sizevec=size2;
    PSD_short = zeros(1,sizevec/16);
    noise = zeros(1,sizevec);
    %noiseAv = zeros(1,sizevec/16);
    psd_num = 1000;
    for i=1:psd_num
        if(mod(i,100)==0)
            i
        end;
        noise = generateNoise(PSDlong);
        noiseFilt = filter(H, noise);
        noiseAv = averageBy16(noiseFilt);
        noiseAvFFT = fft(noiseAv);
        PSD_short = PSD_short + abs(noiseAvFFT).^2;
    end
    PSD_short = PSD_short./psd_num;
end