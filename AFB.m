function out = AFB(x)
    gm = 36.e-3;
    Rl = 3300.;
    den = 1.+(A0(x)/100.);
    out = (A0(x)*gm*Rl)/den;
end