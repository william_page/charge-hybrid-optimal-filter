%-templateTimeShort here needs to be the averaged template!! it will we used
%to built the OF
%-PSDShort is the PSD generated from averaged noise traces. it will also be
%used to build the OF
%-PSDLong will be used to generate the signals
function out = chargeOFAvFiltered(PSDShortF, PSDLong, templateTimeShortF, templateTimeLong,H)
    size1 = size(PSDShortF);
    sizeShort = max(size1(1), size1(2));
    size2 = size(PSDLong);
    sizeLong = max(size2(1), size2(2));
    templateShortFFT = fft(templateTimeShortF);
    optimalFilter = conj(templateShortFFT)./PSDShortF;
    denominator = 0;
    for i=1:sizeShort
        if(i==1)
            continue;
        end
        denominator = denominator + abs(templateShortFFT(i))^2/PSDShortF(i);
    end
    %signalLong = zeros(1,sizeLong);
    %signalShort = zeros(1,sizeShort); 
    signal_amp = (500)*1.6e-19;
    %signal_amp = 0.;
    signal_num = 10000;
    amps = zeros(signal_num,1);
    %out = zeros(signal_num,1);
    for i=1:signal_num
        if(mod(i,100)==0)
            i
        end;
        signalLong = generateNoise(PSDLong);
        signalLong = signalLong+signal_amp*templateTimeLong;
        signalLongF = filter(H, signalLong);
        signalShortF = averageBy16(signalLongF);
        signalShortFFT = fft(signalShortF);
        numerator = 0;
        for j=1:sizeShort
            if(j==1)
                continue;
            end;
        numerator = numerator + optimalFilter(j)*signalShortFFT(j);
        end
        amps(i) = numerator/denominator;
    end
    out = real(amps);
    %histogram(real(amps));
    expectedRMS = sqrt(1/denominator)
    exepectedChargeRMS = expectedRMS/1.6e-19
end