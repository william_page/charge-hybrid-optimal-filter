function out = amp_gain(x,par)
    num = (-1.)*AFB(x)*(1.-B2(x, par));
    den = 1.+AFB(x)*B2(x,par);
    out = num/den;
end