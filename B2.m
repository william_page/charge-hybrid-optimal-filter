function out = B2(x,par)
    num = ZInput(x, par);
    den = ZInput(x, par) + ZFB(x,par);
    out = num/den;
end