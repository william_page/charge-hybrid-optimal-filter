function PSD_long_out = generateLongPSD(PSDlong)
    size1 = size(PSDlong);
    size2 = max(size1(1), size1(2));
    sizevec=size2;
    PSD_long_out = zeros(1,sizevec);
    noise = zeros(1,sizevec);
    psd_num = 5000;
    for i=1:psd_num
        if(mod(i,100)==0)
            i
        end;
        noise = generateNoise(PSDlong);
        noiseFFT = fft(noise);
        PSD_long_out = PSD_long_out + abs(noiseFFT).^2;
    end
    PSD_long_out = PSD_long_out./psd_num;
end