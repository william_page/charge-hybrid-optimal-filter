function out = ZInput(x, par)
  Zdet = complex(0,-1./(x*2*pi*par(1)));
  Zcc = complex(0,-1./(x*2*pi*par(2)));
  Zhemt = complex(0,-1./(x*2*pi*par(3)));
  Rbias = complex(par(4),0); 
  Rbleed = complex(par(5),0);      
  Z50mK = parSum(parSum(Zdet,Rbias) + Zcc,Rbleed);
  Zinput = parSum(Z50mK + Zcc,Zhemt);
             
  out =  Zinput;
end