%-templateTimeShort here needs to be the averaged template!! it will we used
%to built the OF
%-PSDShort is the PSD generated from averaged noise traces. it will also be
%used to build the OF
%-PSDLong will be used to generate the signals
function out = chargeOFAv(PSDShort, PSDLong, templateTimeShort, templateTimeLong)
    PSDShort = PSDShort.*(1/19.07);
    PSDLong = PSDLong./19.07;
    size1 = size(PSDShort);
    sizeShort = max(size1(1), size1(2));
    size2 = size(PSDLong);
    sizeLong = max(size2(1), size2(2));
    %{
    templateShortFFT = fft(templateTimeShort);
    templateShortFFTScaled = templateShortFFT;
    %}
    %Apr. 24 addition to scale the template
    templateTimeShort2 = averageBy16(templateTimeLong);
    templateShortFFT = fft(templateTimeShort2);
    %end recent addition
    figure
    loglog(PSDShort, '.')
    title('PSD SHORT');
    optimalFilter = conj(templateShortFFT)./PSDShort;
    denominator = 0;
    for i=1:sizeShort
        if(i==1)
            continue;
        end
        denominator = denominator + abs(templateShortFFT(i))^2/PSDShort(i);
    end
    expectedRMS = sqrt(1/denominator)
    exepectedChargeRMS = expectedRMS/1.6e-19
    %signalLong = zeros(1,sizeLong);
    %signalShort = zeros(1,sizeShort); 
    signal_amp = (100)*1.6e-19;
    %signal_amp = 0.;
    signal_num = 10000;
    amps = zeros(signal_num,1);
    %out = zeros(signal_num,1);
    %PSD_new = zeros(1,sizeShort);
    for i=1:signal_num
        if(mod(i,100)==0)
            %figure;
            %plot(signalShort, '.')
            %title('signal Short');
            current_res_ENC = std(real(amps(1:i))./1.6e-19)
            current_mean_ENC = mean(real(amps(1:i))./1.6e-19)
            i
        end;
        signalLong = generateNoise(PSDLong);
        signalLong = signalLong+signal_amp*templateTimeLong;
        signalLongScaled = signalLong;%*sqrt(19.07);
        signalShort = averageBy16(signalLongScaled);
        signalShortFFT = fft(signalShort);
        %PSD_new = PSD_new + abs(signalShortFFT).^2;
        signalShortFFTScaled = signalShortFFT;
        numerator = 0;
        for j=1:sizeShort
            if(j==1)
                continue;
            end;
        numerator = numerator + optimalFilter(j)*signalShortFFTScaled(j);
        end
        amps(i) = numerator/denominator;
    end
    %PSD_new = PSD_new./signal_num;
    %figure
    %loglog(PSD_new, '.')
    %title('PSD NEW');
    out = real(amps);
    %histogram(real(amps));
    expectedRMS = sqrt(1/denominator)
    exepectedChargeRMS = expectedRMS/1.6e-19
end