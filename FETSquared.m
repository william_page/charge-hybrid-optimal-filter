function out = FETSquared(x)
%this is for 100M resistors
    out = 4.9e-15;
    %this is for 500M resistors
    out = 6.4e-15;
end