function out = generateNoise(PSD)
    size1 = size(PSD);
    size2 = max(size1(1), size1(2));
    %sizevec = (size2-1)*2;
    sizevec = size2;
    out = zeros(1,sizevec);
    FFT = complex(zeros(1,sizevec), zeros(1,sizevec));
    real_part = 0;
    imag_part = 0;
    nyquist = sizevec/2+1;
    for i=1:sizevec
        if(i==1)
            %note the factor of 2!
            real_part = randn*sqrt(PSD(2)/2);
            FFT(i) = complex(real_part, 0);
        elseif(i<nyquist)
            %note the factor of 2!
            real_part = randn*sqrt(PSD(i)/2);
            imag_part = randn*sqrt(PSD(i)/2);
            FFT(i) = complex(real_part, imag_part);
        elseif(i==nyquist)
            %note the factor of 2!
            real_part = randn*sqrt(PSD(i)/2);
            FFT(i) = complex(real_part, 0);
        else
            FFT(i) = complex(0,0);
        end
    end
    out = ifft(FFT, 'symmetric');
end