function out = averageBy16(in)
    size1 = size(in);
    size2 = max(size1(1), size1(2));
    out = zeros(1,size2/16);
    counter = 1;
    lower_index = 0;
    upper_index = 0;
    for i=1:size2/16
        lower_index = (i-1)*16+1;
        upper_index = (i)*16;
        out(i) = mean(in(lower_index:upper_index));
    end
end