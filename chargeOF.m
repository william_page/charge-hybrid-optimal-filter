function out = chargeOF(PSD, template_time)
    size1 = size(PSD);
    size2 = max(size1(1), size1(2));
    sizevec = size2;
%    template_time = template_time*sqrt(19.07);
    PSD = PSD/19.07;
    templatefft = fft(template_time);
    optimalFilter = conj(templatefft)./PSD;
    denominator = 0;
    for i=1:size2
        if(i==1)
            continue;
        end
        denominator = denominator + abs(templatefft(i))^2/PSD(i);
    end
    expectedRMS = sqrt(1/denominator)
    exepectedChargeRMS = expectedRMS/1.6e-19
    signal = zeros(1,sizevec);
    signal_amp = (100)*1.6e-19;
    %signal_amp = 0.;
    signal_num = 10000;
    amps = zeros(signal_num,1);
    out = zeros(signal_num,1);
    for i=1:signal_num
        if(mod(i,100)==0)
            %figure
            %plot(signal, '.')
            current_res_ENC = std(real(amps(1:i))./1.6e-19)
            current_mean_ENC = mean(real(amps(1:i))./1.6e-19)
            i
        end;
        signal = generateNoise(PSD);
        signal = signal+signal_amp*template_time;
        
        fftsignal = fft(signal);
        numerator = 0;
        for j=1:size2
            if(j==1)
                continue;
            end;
        numerator = numerator + optimalFilter(j)*fftsignal(j);
        end
        amps(i) = numerator/denominator;
    end
    amps_real = real(amps);
    out = amps_real;
    %histogram(amps_real);
    expectedRMS = sqrt(1/denominator)
    exepectedChargeRMS = expectedRMS/1.6e-19
    monteCarloRMS = std(out./1.6e-19)
end