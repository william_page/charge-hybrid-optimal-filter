function out = JohnsonSquared(x, par)
    k = 1.3806488e-23;
    Tmc = 50e-3;
    Tfb = 4.;
    gainSquared = abs(amp_gain(x, par))^2;
    firstTerm = 4.*k*Tmc*realZInput(x, par)*gainSquared;
    secondTerm = 4.*k*Tfb*real(ZFB(x, par));
    out = firstTerm + secondTerm;
end