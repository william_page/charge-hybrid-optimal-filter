function out = A0(x)
  den = (1.+x/10.)*(1.+x/1.e6);
  out = 1e6/den;
end