function out = Vout(x,par)
  out = amp_gain(x, par)*ZInput(x,par);
end