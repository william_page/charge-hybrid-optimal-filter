function out = HEMTSquared(x)
    out = (0.24e-9^2)*(1.+1215./x);
end