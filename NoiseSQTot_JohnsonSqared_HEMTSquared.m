function out = NoiseSQTot_JohnsonSquared_HEMTSquared(x,par)
    %out = HEMTSquared(x)+JohnsonSquared(x,par);
    out = HEMTSquaredGain(x,par)+JohnsonSquared(x,par);
    %add in an oscillation
    %out = out + 2e-14*cos((x/1e2 - 2*pi/16)).^2;
end